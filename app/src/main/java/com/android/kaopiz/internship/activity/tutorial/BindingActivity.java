package com.android.kaopiz.internship.activity.tutorial;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.kaopiz.internship.R;
import com.android.kaopiz.internship.activity.CustomAcitivity;
import com.android.kaopiz.internship.adapter.BindingAdapter;

import java.util.ArrayList;
import java.util.List;

public class BindingActivity extends CustomAcitivity {
    private RecyclerView mCourseList;
    private List<String> mData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        mCourseList = (RecyclerView) findViewById(R.id.course_recycler);
        mCourseList.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        mCourseList.setHasFixedSize(false);
        setTitle("Binding Activity");
        setData();
    }
    public void setData(){
        mData = new ArrayList<>();
        mData.add("Android binding item");
        mData.add("Android binding item");
        mData.add("Android binding item");
        mData.add("Android binding item");
        mData.add("Android binding item");
        mData.add("Android binding item");
        mData.add("Android binding item");
        mData.add("Android binding item");
        mData.add("Android binding item");
        mData.add("Android binding item");
        mData.add("Android binding item");
        mData.add("Android binding item");
        mCourseList.setAdapter(new BindingAdapter(mData));
    }
}
