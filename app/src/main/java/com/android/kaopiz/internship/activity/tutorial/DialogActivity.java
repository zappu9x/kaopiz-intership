package com.android.kaopiz.internship.activity.tutorial;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.kaopiz.internship.R;
import com.android.kaopiz.internship.activity.CustomAcitivity;
import com.android.kaopiz.internship.dialog.CustomDialogFragment;

public class DialogActivity extends CustomAcitivity {
    private Button alert, custom, fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        alert = (Button) findViewById(R.id.dialog_alert);
        alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog();
            }
        });
        custom = (Button) findViewById(R.id.dialog_default);
        custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCustomViewDialog();
            }
        });
        fragment = (Button) findViewById(R.id.dialog_fragment);
        fragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogFragment();
            }
        });
        setTitle("Dialog Activity");
    }
    public void showAlertDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Alert Dialog")
                .setMessage("Android custom dialog example!")
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        dialog.show();
    }
    public void showCustomViewDialog(){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_custom_dialog);
        dialog.setTitle("Custom Dialog");
        TextView text = (TextView) dialog.findViewById(R.id.message);
        text.setText("Android custom dialog example!");
        Button dialogButton = (Button) dialog.findViewById(R.id.button);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void showDialogFragment(){
        CustomDialogFragment dialog = new CustomDialogFragment();
        dialog.show(getFragmentManager(), "AAA");
        dialog.setTitle("Fragment Dialog");
        dialog.setMessage("Android custom dialog example!");
    }

}
