package com.android.kaopiz.internship.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.kaopiz.internship.R;

import java.util.List;

/**
 * Created by nguye on 9/1/2016.
 */
public class TutorialAdapter extends RecyclerView.Adapter<TutorialAdapter.ViewHolder>{
    public List<String> dataList;
    public Context context;
    private OnItemClickListener onItemClickListener;
    public TutorialAdapter(Context context, List<String> dataList) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public TutorialAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final TutorialAdapter.ViewHolder holder, int position) {
        holder.detailView.setText(dataList.get(position));
        holder.courseView.setText("Phần " + (position+1));
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onItemClickListener!= null) onItemClickListener.onClick(view, holder);
            }
        });
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView detailView;
        public TextView courseView;
        public LinearLayout itemLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            detailView = (TextView) itemView.findViewById(R.id.detail_text);
            courseView = (TextView) itemView.findViewById(R.id.course_text);
            itemLayout = (LinearLayout) itemView.findViewById(R.id.list_item);
        }
    }

    public interface OnItemClickListener{
        void onClick(View view, TutorialAdapter.ViewHolder holder);
    }
}
