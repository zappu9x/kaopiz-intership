package com.android.kaopiz.internship.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.android.kaopiz.internship.BR;
import com.android.kaopiz.internship.R;
import com.android.kaopiz.internship.model.Course;

import java.util.List;

/**
 * Created by nguye on 9/1/2016.
 */
public class BindingAdapter extends RecyclerView.Adapter<BindingAdapter.ViewHolder>{
    public List<String> dataList;
    public BindingAdapter(List<String> dataList) {
        this.dataList = dataList;
    }

    @Override
    public BindingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_binding, parent, false);
        ViewHolder viewHolder = new ViewHolder(viewDataBinding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final BindingAdapter.ViewHolder holder, int position) {
        Course course = new Course("Item " + (position + 1),dataList.get(position));
        holder.mViewDataBinding.setVariable(BR.course, course);
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        public ViewDataBinding mViewDataBinding;
        public ViewHolder(ViewDataBinding viewDataBinding) {
            super(viewDataBinding.getRoot());
            mViewDataBinding = viewDataBinding;
            mViewDataBinding.executePendingBindings();
        }
    }
}
