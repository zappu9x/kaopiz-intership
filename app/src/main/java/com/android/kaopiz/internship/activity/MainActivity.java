package com.android.kaopiz.internship.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.kaopiz.internship.adapter.MainAdapter;
import com.android.kaopiz.internship.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView mCourseListView;
    private List<String> mData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCourseListView = (ListView) findViewById(R.id.course_list);
        setData();
    }

    public void setData(){
        mData = new ArrayList<>();
        mData.add("Android fundamental tutorials");
        mData.add("Using Fragments and the ActionBar");
        mData.add("Android file and network access and asynchronous processing");
        mData.add("Android Advanced User Interface Development");
        mData.add("Android Services, BroadcastReceiver and NotificationManager");
        mData.add("Android dependency injection, testing and tools");
        mData.add("Android Location API and Google Maps");
        mData.add("Android Sensors and Touch");
        mData.add("Special Android Programming Topics");
        mCourseListView.setAdapter(new MainAdapter(this, mData));
        mCourseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        startActivity(new Intent(MainActivity.this, TutorialActivity.class));
                        break;
                    case 1:
                        break;
                }
            }
        });
    }
}
