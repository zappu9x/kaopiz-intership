package com.android.kaopiz.internship.activity.tutorial;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.android.kaopiz.internship.R;
import com.android.kaopiz.internship.activity.CustomAcitivity;
import com.android.kaopiz.internship.adapter.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends CustomAcitivity {
    private RecyclerView mCourseList;
    private List<String> mData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        mCourseList = (RecyclerView) findViewById(R.id.course_recycler);
        mCourseList.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        mCourseList.setHasFixedSize(false);
        setTitle("RecyclerView Activity");
        setData();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.recycler_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.grid_layout:
                mCourseList.setLayoutManager(new GridLayoutManager(getBaseContext(), 2));
                return true;
            case R.id.linear_layout:
                mCourseList.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void setData(){
        mData = new ArrayList<>();
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mData.add("Android recycler item");
        mCourseList.setAdapter(new RecyclerAdapter(mData));
    }
}
