package com.android.kaopiz.internship.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.android.kaopiz.internship.R;

/**
 * Created by nguye on 9/6/2016.
 */
public class CustomDialogFragment extends DialogFragment {
    private TextView mTitle, mMessage;
    private String title;
    private String message;

    // Empty constructor required for DialogFragment
    public CustomDialogFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_dialog_fragment, container);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mTitle = (TextView) view.findViewById(R.id.title);
        mTitle.setText(title);
        mMessage = (TextView) view.findViewById(R.id.message);
        mMessage.setText(message);
        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }
}
