package com.android.kaopiz.internship.activity.tutorial;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.android.kaopiz.internship.R;
import com.android.kaopiz.internship.activity.CustomAcitivity;

public class IntentSubActivity extends CustomAcitivity {
    public static String DATA_KEY = "DATA_RESULT";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intentsub);
        setTitle("Intent Sub Activity");
        Intent intent = getIntent();
        String output = intent.getStringExtra(IntentActivity.DATA_KEY);
        ((TextView) findViewById(R.id.data_view)).setText(output);
    }

    @Override
    public void finish() {
        Intent data = new Intent();
        data.putExtra("DATA_RESULT", ((EditText) findViewById(R.id.data_input)).getText().toString());
        setResult(RESULT_OK, data);
        super.finish();
    }
}
