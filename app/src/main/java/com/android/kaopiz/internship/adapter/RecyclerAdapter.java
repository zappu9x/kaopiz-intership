package com.android.kaopiz.internship.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.kaopiz.internship.R;

import java.util.List;

/**
 * Created by nguye on 9/1/2016.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>{
    public List<String> dataList;
    public RecyclerAdapter(List<String> dataList) {
        this.dataList = dataList;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapter.ViewHolder holder, int position) {
        holder.contentView.setText("Item " + (position + 1));
        holder.detailView.setText(dataList.get(position));
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                remove(holder.getAdapterPosition());
            }
        });
    }

    public void insert(String data, int position){
        dataList.add(position, data);
        notifyItemInserted(position);
    }
    public void remove(int position){
        notifyItemRemoved(position);
        dataList.remove(position);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView contentView, detailView;
        public LinearLayout itemLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            contentView = (TextView) itemView.findViewById(R.id.course_text);
            detailView = (TextView) itemView.findViewById(R.id.detail_text);
            itemLayout = (LinearLayout) itemView.findViewById(R.id.list_item);
        }
    }
}
