package com.android.kaopiz.internship.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.kaopiz.internship.R;
import com.android.kaopiz.internship.activity.tutorial.BindingActivity;
import com.android.kaopiz.internship.activity.tutorial.DialogActivity;
import com.android.kaopiz.internship.activity.tutorial.IntentActivity;
import com.android.kaopiz.internship.activity.tutorial.LoggingActivity;
import com.android.kaopiz.internship.activity.tutorial.RecyclerActivity;
import com.android.kaopiz.internship.adapter.TutorialAdapter;

import java.util.ArrayList;
import java.util.List;

public class TutorialActivity extends CustomAcitivity {
    private RecyclerView mCourseList;
    private List<String> mData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        mCourseList = (RecyclerView) findViewById(R.id.course_recycler);
        mCourseList.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        mCourseList.setHasFixedSize(false);
        setTitle("Tutorial Activity");
        setData();
    }
    public void setData(){
        mData = new ArrayList<>();
        mData.add("Android Intents");
        mData.add("Using lists and grids in Android with RecylerView");
        mData.add("Using Databinding in Android applications");
        mData.add("Android logging");
        mData.add("Showing dialogs in Android with fragments");
        TutorialAdapter adapter = new TutorialAdapter(getBaseContext(), mData);
        adapter.setOnItemClickListener(new TutorialAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, TutorialAdapter.ViewHolder holder) {
                Intent intent;
                switch (holder.getAdapterPosition()){
                    case 0:
                        intent = new Intent(TutorialActivity.this, IntentActivity.class);
                        break;
                    case 1:
                        intent = new Intent(TutorialActivity.this, RecyclerActivity.class);
                        break;
                    case 2:
                        intent = new Intent(TutorialActivity.this, BindingActivity.class);
                        break;
                    case 3:
                        intent = new Intent(TutorialActivity.this, LoggingActivity.class);
                    case 4:
                        intent = new Intent(TutorialActivity.this, DialogActivity.class);
                        break;
                    default:
                        intent = null;
                }
                if(intent != null) startActivity(intent);
            }
        });
        mCourseList.setAdapter(adapter);
    }
}
