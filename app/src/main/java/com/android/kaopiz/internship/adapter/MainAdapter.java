package com.android.kaopiz.internship.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.kaopiz.internship.R;
import com.android.kaopiz.internship.activity.TutorialActivity;

import java.util.List;

/**
 * Created by nguye on 8/30/2016.
 */
public class MainAdapter extends ArrayAdapter{

    private List<String> mData;
    private LayoutInflater mInflater;

    public MainAdapter(Context context, List<String> data) {
        super(context, 0, data);
        mData = data;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.item_main, null);
        }
        TextView courseView = (TextView) view.findViewById(R.id.course_text);
        courseView.setText("Bài " + (position + 1));
        TextView detailView = (TextView) view.findViewById(R.id.detail_text);
        detailView.setText(mData.get(position));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("Position", position);
                Intent intent = new Intent(getContext(), TutorialActivity.class);
                intent.putExtras(bundle);
                getContext().startActivity(intent);
            }
        });
        return view;
    }
}
