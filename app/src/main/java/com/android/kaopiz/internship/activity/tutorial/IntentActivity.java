package com.android.kaopiz.internship.activity.tutorial;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.kaopiz.internship.R;
import com.android.kaopiz.internship.activity.CustomAcitivity;

public class IntentActivity extends CustomAcitivity implements View.OnClickListener {
    private Button custom, system;
    private EditText input;
    private TextView output;
    public static String DATA_KEY = "DATA";
    public static int REQUEST_CODE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);
        custom = (Button) findViewById(R.id.custom_button);
        system = (Button) findViewById(R.id.system_button);
        input = (EditText) findViewById(R.id.data_input);
        output = (TextView) findViewById(R.id.data_result);
        setTitle("Intent Activity");
        custom.setOnClickListener(this);
        system.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(IntentActivity.this, IntentSubActivity.class);
        switch (view.getId()){
            case R.id.custom_button:
                /*Bundle bundle = new Bundle();
                bundle.putString(DATA_KEY, input.getText().toString());
                intent.putExtras(bundle);*/
                //Cach 2
                intent.putExtra(DATA_KEY, input.getText().toString());
                startActivityForResult(intent, REQUEST_CODE);
                break;
            case R.id.system_button:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com")));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK && requestCode == REQUEST_CODE){
            String text = data.getStringExtra(IntentSubActivity.DATA_KEY);
            output.setText(text);
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }
}
